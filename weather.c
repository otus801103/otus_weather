#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>
#include <cjson/cJSON.h>

//структура с указателем на память и размером
typedef struct {
    char *memory;
    size_t size;
} MemoryStruct;


// Обрытный вызов и запись данных в память
static size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp) {
    size_t realsize = size * nmemb;
    MemoryStruct *mem = (MemoryStruct *)userp;

    char *ptr = realloc(mem->memory, mem->size + realsize + 1);
    if (!ptr) {
        printf("not enough memory (realloc returned NULL)\n");
        return 0;
    }

    mem->memory = ptr;
    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;

    return realsize;
}

int main(int argc, char *argv[]) {
    // проверка наличия аргументов в командной строке
    if (argc < 2) {
        fprintf(stderr, "Usage: %s <city>\n", argv[0]);
        return 1;
    }
    
    char queryUrl[256];//Массив символов для хранения URL-адреса запроса
    snprintf(queryUrl, sizeof(queryUrl), "http://wttr.in/%s?format=j1", argv[1]);

    CURL *curl_handle;// Указатель на объект CURL, который используется для выполнения HTTP-запроса.
    CURLcode res;//Определяем переменную для хранения результата операции CURL.

    // Опеределяем объект для полученых данных
    MemoryStruct chunk;
    chunk.memory = malloc(1);
    chunk.size = 0;
    
    //Инициализируем библиотеку libcurl
    curl_global_init(CURL_GLOBAL_ALL);
    //Создаём новый объект CURL для выполнения HTTP-запроса
    curl_handle = curl_easy_init();

    //Устанавливаем URL-адрес запроса.
    curl_easy_setopt(curl_handle, CURLOPT_URL, queryUrl);
    //Устанавливаем функцию обратного вызова для записи данных в память.
    curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
    //Устанавливаем объект структуры MemoryStruct для записи полученных данных
    curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)&chunk);
    //Устанавливаем значение заголовка User-Agent для запроса
    curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");

    res = curl_easy_perform(curl_handle);//Выполянем запроси сохраняем резултат в переменной res
    // Проверка успешности выполнения запроса
    if (res != CURLE_OK) {
        fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
    } else {
        //Разбираем полученные данные в формате JSON
        cJSON *json = cJSON_Parse(chunk.memory);
        //Проверка успешности разборки JSON
        if (json == NULL) {
            const char *error_ptr = cJSON_GetErrorPtr();
            if (error_ptr != NULL) {
                fprintf(stderr, "Error before: %s\n", error_ptr);
            }
        } else {
            //Получаем объект JSON с именем "current_condition имя определено на wttr.inд".
            cJSON *weather_current_condition = cJSON_GetObjectItemCaseSensitive(json, "current_condition");
            if (weather_current_condition != NULL) {
            //Получаем массив JSON с именем "current_condition"
            cJSON *weather_current_condition_array = cJSON_GetObjectItemCaseSensitive(json, "current_condition");
            if (weather_current_condition_array != NULL && cJSON_IsArray(weather_current_condition_array))
               {
               //Получаем первый элемент массива JSON
               cJSON *weather_current_condition = cJSON_GetArrayItem(weather_current_condition_array, 0);
               //Получаем значение JSON с именем "temp_C,winddir16Point,windspeedKmph,weatherDesc"
               cJSON *temp_C = cJSON_GetObjectItemCaseSensitive(weather_current_condition, "temp_C");
               cJSON *winddir16Point = cJSON_GetObjectItemCaseSensitive(weather_current_condition, "winddir16Point");
               cJSON *windspeedKmph = cJSON_GetObjectItemCaseSensitive(weather_current_condition, "windspeedKmph");
               cJSON *weatherDesc = cJSON_GetObjectItemCaseSensitive(weather_current_condition, "weatherDesc");
               //Получаем значение JSON с подчинённое значение weatherDesc;
               cJSON *desc = cJSON_GetObjectItemCaseSensitive(weatherDesc->child, "value");
               // Выводим полученные значения
               fprintf(stdout, "Weather Description: %s\n", desc->valuestring);
               fprintf(stdout, "Current Temperature: %s°C\n", temp_C->valuestring);
               fprintf(stdout, "Wind Direction: %s Speed: %ld Mps\n", winddir16Point->valuestring,(strtol(windspeedKmph->valuestring, NULL, 10))*1000/60/60);
               }
            }
            //Освобождаем память
            cJSON_Delete(json);
        }
    }
    //Освобождает ресурсы
    curl_easy_cleanup(curl_handle);
    //Освобождает память, занятую буфером данных
    free(chunk.memory);
    //Завершаем работу с библиотекой libcurl
    curl_global_cleanup();

    return 0;
}

